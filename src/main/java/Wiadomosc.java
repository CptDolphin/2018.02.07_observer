public class Wiadomosc {
    private String wiadomosc;
    private String numer;

    public Wiadomosc(String wiadomosc, String numer) {
        this.wiadomosc = wiadomosc;
        this.numer = numer;
    }

    public String getWiadomosc() {
        return wiadomosc;
    }

    public void setWiadomosc(String wiadomosc) {
        this.wiadomosc = wiadomosc;
    }

    public String getNumer() {
        return numer;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }
}
