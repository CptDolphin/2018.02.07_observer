package NewsStation;

import java.util.Observable;

public class NewsStation extends Observable {

    public NewsStation() {
    }

    public void addNews(String wiadomosc, int poziom) {
        News news = new News(wiadomosc, poziom);
        setChanged();
        notifyObservers(news);
    }
}
