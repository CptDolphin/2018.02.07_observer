package NewsStation;

public class News {
    private String wiadomosc;
    private int poziom;

    public News(String wiadomosc, int poziom) {
        this.wiadomosc = wiadomosc;
        this.poziom = poziom;
    }

    public String getWiadomosc() {
        return wiadomosc;
    }

    public void setWiadomosc(String wiadomosc) {
        this.wiadomosc = wiadomosc;
    }

    public int getPoziom() {
        return poziom;
    }

    public void setPoziom(int poziom) {
        this.poziom = poziom;
    }
}