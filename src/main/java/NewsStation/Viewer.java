package NewsStation;

import java.util.Observable;
import java.util.Observer;

public class Viewer implements Observer {
    private int newsThreshold;
    private String name;

    public Viewer(int newsThreshold, String name) {
        this.newsThreshold = newsThreshold;
        this.name = name;
    }

    public void update(Observable o, Object arg) {
        if(arg instanceof News ){
            News wiadomosc = (News) arg;
            if(wiadomosc.getPoziom() >= newsThreshold){
                System.out.println("Panic! " + wiadomosc.getWiadomosc() + " " + name + " wpada w szał");
            }
        }
    }
}
