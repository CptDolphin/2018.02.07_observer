package zadanie_1_smsStation;

import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {

    private String numer;

    public Phone(String numer) {
        this.numer = numer;
    }

    public String getNumer() {
        return numer;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }

    public void update(Observable o, Object arg) {
        if (arg instanceof Wiadomosc) {
            Wiadomosc wiadomosc = (Wiadomosc) arg;
            if (wiadomosc.getNumer().equals(this.numer)) {
                System.out.println("Wiadomosc brzmi: " + wiadomosc + " od: " + numer + " numeru telefonu.");
            }
        }
    }
}