package zadanie_1_smsStation;

import zadanie_1_smsStation.Phone;

import java.util.Observable;

public class SmSStation extends Observable {

    public SmSStation() {
    }

    public void addPhone(Phone p) {
        addObserver(p);
    }

    public void sendSms(String number, String tresc) {
        Wiadomosc wiadomosc = new Wiadomosc(number, tresc);
        setChanged();
//        notifyObservers(wiadomosc);
    }
}
