package zadanie_1_smsStation;

public class Main {
    public static void main(String[] args) {
        SmSStation smSStation = new SmSStation();
        Phone phone1 = new Phone("518420103");
        Phone phone2 = new Phone("881535887");

        smSStation.addPhone(phone1);
        smSStation.addPhone(phone2);

        smSStation.sendSms("Makarena", "881535887");
    }
}
