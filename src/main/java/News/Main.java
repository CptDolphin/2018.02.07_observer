package News;

public class Main {
    public static void main(String[] args) {
        NewsStation station= new NewsStation();

        Viewer viewer1 = new Viewer(5,"Tomek");
        Viewer viewer2 = new Viewer(11,"Ryszard");

        station.addObserver(viewer1);
        station.addObserver(viewer2);

        station.addNews("Bialy dom sie pali:", 10);
    }
}
