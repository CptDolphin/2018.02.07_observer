package News;

public class News {
    private String wiadomocs;
    private int poziom;

    public News(String wiadomocs, int poziom) {
        this.wiadomocs = wiadomocs;
        this.poziom = poziom;
    }

    public String getWiadomocs() {
        return wiadomocs;
    }

    public void setWiadomocs(String wiadomocs) {
        this.wiadomocs = wiadomocs;
    }

    public int getPoziom() {
        return poziom;
    }

    public void setPoziom(int poziom) {
        this.poziom = poziom;
    }
}
