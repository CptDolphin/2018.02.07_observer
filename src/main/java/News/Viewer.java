package News;

import java.util.Observable;
import java.util.Observer;

public class Viewer implements Observer {
    private int poziom;
    private String imie;

    public Viewer(int poziom, String imie) {
        this.poziom = poziom;
        this.imie = imie;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getPoziom() {
        return poziom;
    }

    public void setPoziom(int poziom) {
        this.poziom = poziom;
    }

    public void update(Observable o, Object arg) {
        if (arg instanceof News) {
            News news = (News) arg;
            if(news.getPoziom()>this.poziom){
                System.out.println("O nie! " + news.getWiadomocs() +  " zobaczyl" + this.imie);
            }
        }
    }
}
